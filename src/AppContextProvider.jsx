import React, { useContext } from "react";
import { immutable } from "./utils";

const DefaultAppValue = {
    activeComponents: []
}

export const AppContext = React.createContext();


export class AppContextProvider extends React.Component {

    state = DefaultAppValue;

    /**
     * @param {function (WrappedObject<T>)} updater
     */
    updateState = (updater) => {
        const newState = immutable.wrap(this.state);
        updater(newState);
        this.setState(newState.value())
    }

    render() {

        return (
            <AppContext.Provider value={{ app: this.state, updateState: this.updateState }}>
                {this.props.children}
            </AppContext.Provider >
        )
    }

}

export function useAppState() {
    return useContext(AppContext);
}
