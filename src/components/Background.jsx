import React, { useRef } from "react";
import { useInterval } from "../hooks/useInterval";
import { useWindowSize } from "../hooks/useWindowSize";
//import { useAppState } from "../AppContextProvider";

export const ComponentNameBackgroundParticles = "BackgroundParticles";
export const ComponentNameBackgroundConnection = "BackgroundConnection";
export const ComponentNameBackgroundInteraction = "BackgroundInteraction";



export function Background() {

    //const { app } = useAppState();
    const canvasRef = useRef(null);
    const [width, height] = useWindowSize();




    let ctx = null;

    let particles = [];
    for (let i = 0; i < 100; i++) {
        particles.push({ id: particles.length, x: Math.random() * width, y: Math.random() * height, vx: Math.random() * 0.0001, vy: Math.random() * 0.0001, color: "#ff00ff", m: -5 + Math.random() * 10 })
    }

    particles.push({ id: particles.length, x: Math.random() * width, y: Math.random() * height, vx: Math.random() * 0.0001, vy: Math.random() * 0.0001, color: "#ff00ff", static: true, m: -100 })

    // if (!app.activeComponents.includes(ComponentNameBackground))
    //     return null;

    let enableParticles = true;// app.activeComponents.includes(ComponentNameBackgroundParticles);
    let enableConnections = true; //app.activeComponents.includes(ComponentNameBackgroundConnection);
    //let enableInteraction = app.activeComponents.includes(ComponentNameBackgroundInteraction);

    useInterval(() => {
        if (canvasRef.current === null)
            return;

        updateCanvasSize(canvasRef.current, width, height);

        if (ctx === null) {
            ctx = canvasRef.current.getContext('2d');
            canvasRef.current.addEventListener("mousemove", e => {
                particles[particles.length - 1].x = e.offsetX - 10;
                particles[particles.length - 1].y = e.offsetY - 10;
            }, false);
        }
        ctx.setTransform(1, 0, 0, 1, 0, 0);
        ctx.clearRect(0, 0, canvasRef.current.width, canvasRef.current.height);

        let connections = [];

        if (!enableParticles)
            return;

        animateParticles(particles, connections, width, height);

        if (enableConnections) {
            drawConnections(ctx, connections, width, height);
        }

        drawParticles(ctx, particles);

    }, 1000 / 60)






    return (
        <canvas ref={canvasRef} className="background-container" >

        </canvas>
    );
}

function updateCanvasSize(canvas, width, height) {

    if (canvas != null && (canvas.width !== width || canvas.height !== height)) {
        canvas.width = width;
        canvas.height = height;
    }
}

function animateParticles(particles, connections, width, height) {

    for (let aIndex in particles) {
        if (particles[aIndex].static)
            continue;

        if (particles[aIndex].x > width + 20) particles[aIndex].x = 0;
        if (particles[aIndex].x < -20) particles[aIndex].x = width;
        if (particles[aIndex].y > height + 20) particles[aIndex].y = 0;
        if (particles[aIndex].y < -20) particles[aIndex].y = height;


        let tempConnections = [];

        for (let bIndex in particles) {
            if (aIndex === bIndex)
                continue;

            let particleA = particles[aIndex];


            let particleB = particles[bIndex];

            const [distanceX, distanceY] = getSmallestDistance(particleA, particleB, width, height);

            const disSquared = distanceX * distanceX + distanceY * distanceY;
            const distance = Math.sqrt(disSquared);

            if (distance < 90) {
                if (null == tempConnections.find(c => (c.a.id === particleA.id && c.b.id === particleB.id) || (c.a.id === particleB.id && c.b.id === particleA.id)))
                    tempConnections.push({ a: particleA, b: particleB, distance });
            }


            let force = 1 * particleA.m * particleB.m / disSquared;
            const unitX = distanceX / distance;
            const unitY = distanceY / distance;

            //if (!particleA.static && !particleB.static && distance < 50) {
            if (Math.sign(particleA.m) === Math.sign(particleB.m) && distance < 50) {
                force *= -1
            }

            particleA.vx += force * unitX * 0.1;
            particleA.vy += force * unitY * 0.1;

            const MaxSpeed = 1;

            if (particleA.vx < -MaxSpeed) particleA.vx = -MaxSpeed;
            if (particleA.vy < -MaxSpeed) particleA.vy = -MaxSpeed;
            if (particleA.vx > MaxSpeed) particleA.vx = MaxSpeed;
            if (particleA.vy > MaxSpeed) particleA.vy = MaxSpeed;

            particleA.vx *= 0.999;
            particleA.vy *= 0.999;

            particleA.x += particleA.vx;
            particleA.y += particleA.vy;

        }
        tempConnections = tempConnections.sort((a, b) => a.distance - b.distance).slice(0, Math.max(tempConnections.length-2, 3));


        connections.push(...tempConnections);

    }

}

function getSmallestDistance(particleA, particleB, width, height) {

    let distX = AbsMin(
        particleB.x - particleA.x,
        particleB.x - particleA.x + width,
        particleB.x - particleA.x - width
    );

    let distY = AbsMin(
        particleB.y - particleA.y,
        particleB.y - particleA.y + height,
        particleB.y - particleA.y - height
    );
    return [distX, distY];
}


function AbsMin(...args) {
    let min = args[0];
    for (let i = 1; i < args.length; i++) {
        if (Math.abs(args[i]) < min)
            min = args[i];
    }
    return min;
}


function drawParticles(ctx, particles) {


    for (const p of particles) {
        // if (p.static)
        //     continue;

        let radgrad = ctx.createRadialGradient(10, 10, 0, 10, 10, 10);
        radgrad.addColorStop(0, 'rgba(255,255,255,' + (p.m / 10) + ')');
        radgrad.addColorStop(0.8, 'rgba(200,255,200,.3)');
        radgrad.addColorStop(1, 'rgba(200,255,200,0)');
        ctx.fillStyle = radgrad;

        ctx.setTransform(1, 0, 0, 1, 0, 0);
        ctx.translate(p.x, p.y);
        ctx.fillRect(0, 0, 20, 20);
    }


}

function drawConnections(ctx, connections, width, height) {
    for (const c of connections) {

        ctx.setTransform(1, 0, 0, 1, 0, 0);

        ctx.beginPath();

        const bx = getClosestProp(c, "x", width);
        const by = getClosestProp(c, "y", height);

        ctx.moveTo(c.a.x + 10, c.a.y + 10);
        ctx.lineTo(bx + 10, by + 10);
        ctx.lineWidth = 2;
        ctx.strokeStyle = '#ffffffaa';
        ctx.lineCap = 'butt';
        ctx.stroke();

    }
}

function getClosestProp(connection, propName, shift) {
    let v = connection.b[propName];
    let minDist = Math.abs(connection.b[propName] - connection.a[propName]);

    let tmpDist = Math.abs(connection.b[propName] + shift - connection.a[propName]);

    if (tmpDist < minDist) {
        minDist = tmpDist;
        v = connection.b[propName] + shift;
    }
    tmpDist = Math.abs(connection.b[propName] - shift - connection.a[propName]);
    if (tmpDist < minDist) {
        v = connection.b[propName] - shift;
    }

    return v;
}