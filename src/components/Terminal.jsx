
import React, { Fragment, useState } from "react";
import { useInterval } from "../hooks/useInterval";
import "./Terminal.scss";

export function Terminal({ title = "Robert - homepage", propmpt = "root:~$", commands = [] }) {

    const [state, setState] = useState({ timeout: 30, commandIndex: 0, commandCharacterIndex: 0, printedCommands: [] })

    useInterval(() => {
        let { timeout, commandIndex, commandCharacterIndex, printedCommands } = state;

        if (commandIndex >= commands.length) {
            return setState({ timeout: null, commandIndex, commandCharacterIndex, printedCommands });
        }

        if (printedCommands.length < commandIndex + 1)
            printedCommands.push("");

        if (commandCharacterIndex < commands[commandIndex].cmd.length) {
            printedCommands[commandIndex] += commands[commandIndex].cmd[commandCharacterIndex];
            commandCharacterIndex++;
        }

        if (commandCharacterIndex === commands[commandIndex].cmd.length) {
            commands[commandIndex].onDone && commands[commandIndex].onDone();
            commandIndex++;
            commandCharacterIndex = 0;
            timeout = 700;
        } else {
            timeout = 20 + Math.random() * 100;
        }

        setState({ timeout, commandIndex, commandCharacterIndex, printedCommands })
    }, state.timeout)

    return (
        <div style={{ margin: "0px 5px 5px 5px", position: "relative", paddingTop: 5, maxWidth: 700 }}>
            <div className="terminalBar" >
                <div className="terminalRed" />
                <div className="terminalYellow" />
                <div className="terminalGreen" />
                <div className="terminalTitle">{title}</div>
            </div >
            <div className="terminalScreen">
                <p className="terminalFont">{state.printedCommands.map((command, index) => <Fragment key={index}>{propmpt} {command} <br /></Fragment>)}</p>
            </div>
        </div>
    )
}