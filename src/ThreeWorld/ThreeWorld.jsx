import React from "react";
import * as THREE from "three";
import { DDSLoader } from "three/examples/jsm/loaders/DDSLoader";
import { FBXLoader } from 'three/examples/jsm/loaders/FBXLoader';


export function ThreeWorld() {

    return <canvas ref={(canvas) => { initThreeWorld(canvas) }} className="background-container" />
}

function initThreeWorld(canvas) {

    var renderer = new THREE.WebGLRenderer({ canvas: canvas });
    var camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
    renderer.setSize(window.innerWidth, window.innerHeight);

    var scene = new THREE.Scene();

    // var geometry = new THREE.BoxGeometry();
    // var material = new THREE.MeshLambertMaterial({ color: 0x00ff00 });
    // var cube = new THREE.Mesh(geometry, material);
    // scene.add(cube);

    camera.position.z = 3;
    //camera.position.y = 2;

    var directionalLight = new THREE.DirectionalLight(0xffffff, 0.5);

    scene.add(directionalLight);

    var ambientLight = new THREE.AmbientLight(0xcccccc, 0.4);
    scene.add(ambientLight);

    var pointLight = new THREE.PointLight(0xffffff, 0.8);
    camera.add(pointLight);



    var manager = new THREE.LoadingManager();
    manager.addHandler(/\.dds$/i, new DDSLoader());

    // var onProgress = function (xhr) {

    //     if (xhr.lengthComputable) {

    //         var percentComplete = xhr.loaded / xhr.total * 100;
    //         console.log(Math.round(percentComplete, 2) + '% downloaded');

    //     }

    // };


    /** @type {THREE.Group} */
    let head = null;

    // let headWidth = 0;
    // let headHeight = 0;
    // let headDepth = 0;
    // let headCenter = null;

    const path = "/homepage";

    var loader = new FBXLoader();
    loader.load(path + "/head3/skull.fbx", function (object) {
        head = object;
        scene.add(object);
    })
    // new MTLLoader(manager)
    //     .setPath('/head/')
    //     .load('head.mtl', function (materials) {

    //         materials.preload();

    //         console.log("materials", materials)

    //         new OBJLoader(manager)
    //             .setMaterials(materials)
    //             .setPath('/head/')
    //             .load('head.obj', function (object) {

    //                 head = object;
    //                 //head.position.y = 10

    //                 // let geometry = object.children[0].geometry;
    //                 // geometry.computeBoundingBox();
    //                 // headCenter = geometry.boundingBox.getCenter();
    //                 // headWidth = geometry.boundingBox.max.x - geometry.boundingBox.min.x;
    //                 // headHeight = geometry.boundingBox.max.y - geometry.boundingBox.min.y;
    //                 // headDepth = geometry.boundingBox.max.z - geometry.boundingBox.min.z;
    //                 // head.position.x = -headWidth / 2;
    //                 // head.position.y = -headHeight / 2;

    //                 //object.children[0].position.x = -2;

    //                 console.log("object.children[0].position", object.children[0].position)

    //                 //let wohead.localToWorld( center );
    //                 //head.translateX(width / 2)

    //                 scene.add(object);
    //                 //camera.lookAt(head.position);

    //             }, onProgress, console.error);

    //     });

    //let mouseX = 0, mouseY = 0;
    var windowHalfX = window.innerWidth / 2;
    var windowHalfY = window.innerHeight / 2;

    document.addEventListener('mousemove', (event) => {

        var mouse3D = new THREE.Vector3((-(event.clientX - windowHalfX) / window.innerWidth) * 2, ((event.clientY - windowHalfY) / window.innerHeight) * 2, 1.01);
        mouse3D = mouse3D.unproject(camera);

        if (head) {
            head.lookAt(mouse3D);
        }

    }, false);


    window.addEventListener('resize', () => {
        windowHalfX = window.innerWidth / 2;
        windowHalfY = window.innerHeight / 2;
        camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();
        renderer.setSize(window.innerWidth, window.innerHeight);
    }, false);


    //var clock = new THREE.Clock();

    const animate = () => {
        requestAnimationFrame(animate);
        // let elapsed = clock.elapsedTime;
        // let delta = clock.getDelta();



        //camera.position.x += (mouseX - camera.position.x) * .0005;
        //camera.position.y += (- mouseY - camera.position.y) * .0005;
        //console.log(delta)
        // if (head) {

        //     head.translateX(10);
        //     head.rotateY(delta * 1);
        //     head.translateX(-10);
        //     //head.scale.x = 1 + Math.sin(elapsed);
        // }


        renderer.render(scene, camera);
    }

    animate();

}

